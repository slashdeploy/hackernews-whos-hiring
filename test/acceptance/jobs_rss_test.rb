require_relative '../test_helper'

class JobAdRSSTest < RSSTest
  def test_filters
    job = JobAd.new({
      id: 1,
      text: 'Foo | Bar',
      timestamp: CLOCK
    })

    remote_job = JobAd.new({
      id: 2,
      text: 'Foo | Bar | REMOTE',
      timestamp: CLOCK + 1
    })

    repo << job << remote_job

    get_rss '/rss' do |feed, items|
      assert_equal 2, items.size
      assert_feed_item remote_job, items[0]
      assert_feed_item job, items[1]
    end

    get_rss '/rss', filter: :remote do |feed, items|
      assert_equal 1, items.size
      assert_feed_item remote_job, items[0]
    end
  end

  def test_keyword
    job_a = JobAd.new({
      id: 1,
      text: 'Foo',
      timestamp: CLOCK
    })

    job_b = JobAd.new({
      id: 2,
      text: 'Bar',
      timestamp: CLOCK + 1
    })

    repo << job_a << job_b

    get_rss '/rss', q: 'Bar' do |feed, items|
      assert_equal 1, items.size
      assert_feed_item job_b, items[0]
    end
  end
end
