require_relative '../test_helper'

class FilterJobsTest < AcceptanceTest
  def test_keyword
    repo << JobAd.new({
      id: 1,
      text: 'Job_A | ONSITE | $60k',
      timestamp: CLOCK
    })

    repo << JobAd.new({
      id: 2,
      text: 'Job_B | REMOTE | $100k',
      timestamp: CLOCK
    })

    open_jobs

    assert_results 2

    search_for 'Job_B'
    assert_keyword 'Job_B'

    assert_results 1
    assert_post 2
  end

  def test_remote_job_filter
    repo << JobAd.new({
      id: 1,
      text: 'Foo | Bar | ONSITE | $60k',
      timestamp: CLOCK
    })

    repo << JobAd.new({
      id: 2,
      text: 'Foo | Bar | REMOTE | $100k<p>More',
      timestamp: CLOCK
    })

    open_jobs

    assert_results 2
    assert_all_jobs_selected

    select_remote

    assert_results 1
    assert_post 2
    assert_remote_selected
    refute_all_jobs_selected

    select_all_posts

    assert_results 2
    assert_all_jobs_selected
    refute_remote_selected
  end

  private

  def select_remote
    select('Remote', from: 'post-filter')
    click_button('get-posts')
  end

  def assert_all_jobs_selected
    assert has_select?('post-filter', selected: 'All'), 'All filter incorrect'
  end

  def refute_all_jobs_selected
    refute has_select?('post-filter', selected: 'All'), 'All filter incorrect'
  end

  def assert_remote_selected
    assert has_select?('post-filter', selected: 'Remote'), 'Remote filter incorrect'
  end

  def refute_remote_selected
    refute has_select?('post-filter', selected: 'Remote'), 'Remote filter incorrect'
  end
end
