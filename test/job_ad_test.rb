require_relative 'test_helper'

class JobAdTest < MiniTest::Test
  def parse(id: 1, text: 'Company | Position | Location | Tags | Salary', timestamp: Time.now)
    JobAd.from_hn({
      'id' => id,
      'text' => text,
      'time' => Time.now
    })
  end

  def test_parsing_scenarios
    [
      [
        %{Company | Title | Location | $100k<p>Bar},
        { remote: false }
      ],
      [
        %{Company | Title | REMOTE | $100k<p>Bar},
        { remote: true }
      ],
      [
        # Decodes HTML Entities
        %{The Farmer&#x27;s Dog | Title | Location | $100k\nMore},
        { remote: false }
      ]
    ].each do |(text, output)|
      job = parse({
        text: text
      })

      assert_equal HTMLEntities.new.decode(text), job.to_s, "#{text} parse failure"

      if output.key?(:remote)
        assert job.remote? == output.fetch(:remote), "#{text} parse failure"
      end
    end
  end
end
