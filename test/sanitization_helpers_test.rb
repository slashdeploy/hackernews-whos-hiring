require_relative './test_helper'

class SanitizationHelpersTest < MiniTest::Test
  include SanitizationHelpers

  def assert_truncated(text)
    assert text.end_with?('...'), 'Text not truncated'
  end

  def job_ad(text)
    JobAd.new({
      id: 1,
      text: text,
      timestamp: Time.now.to_i
    })
  end

  def freelance_ad(text)
    FreelanceAd.new({
      id: 1,
      text: text,
      timestamp: Time.now.to_i
    })
  end

  def titleize_job_ad(text)
    super(job_ad(text))
  end

  def titleize_freelance_ad(text)
    super(freelance_ad(text))
  end

  def test_titleize_job_ad_uses_the_first_line_of_text
    assert_equal 'foo', titleize_job_ad('foo<p>bar')
    assert_equal 'foo', titleize_job_ad('<p>foo<p>bar')
    assert_equal 'foo', titleize_job_ad("foo\nbar")
  end

  def test_titelize_job_ad_strips_tags
    assert_equal 'bar', titleize_job_ad('<a href="/bar">foo</a> bar')
    assert_equal 'bar', titleize_job_ad('<a>foo</a> bar')
    assert_equal 'bar', titleize_job_ad('<a>(foo)</a> bar')
    assert_equal 'bar', titleize_job_ad('<a>( foo )</a> bar')
    assert_equal 'bar', titleize_job_ad('(<a>foo</a>) bar')
    assert_equal 'foo bar', titleize_job_ad('<b>foo</b> bar')
    assert_equal 'foo bar', titleize_job_ad('<i>foo</i> bar')
  end

  def test_titelize_job_ad_remove_empty_pipe_segments
    assert_equal 'foo | bar', titleize_job_ad('foo | | bar ')
    assert_equal 'foo | bar', titleize_job_ad('foo |  | bar ')
    assert_equal 'foo | bar', titleize_job_ad('foo || bar ')
  end

  def test_titlize_job_ad_strips_leading_whitespace
    assert_equal 'foo', titleize_job_ad('  foo')
  end

  def test_titelize_job_ad_as_code_posting
    assert_equal 'foo', titleize_job_ad('<p><pre><code>foo</pre></code>')
  end

  def test_titelize_job_ad_truncates_long_text
    assert_truncated titleize_job_ad('foo' * 100)
  end

  def test_titleize_freelance_ad_strips_keyword
    assert_equal 'Placeholder', titleize_freelance_ad('SEEKING WORK - Placeholder')
    assert_equal 'Placeholder', titleize_freelance_ad('--- SEEKING WORK --- Placeholder')
    assert_equal 'Placeholder', titleize_freelance_ad('---<p>SEEKING WORK --- Placeholder')
    assert_equal 'Placeholder', titleize_freelance_ad('SEEKING WORK --- Placeholder')
  end

  def test_titleize_freelance_ad_joins_by_p_tag
    assert_equal 'foo; bar', titleize_freelance_ad('foo<p>bar')
  end

  def test_titelize_freelance_ad_truncates_long_text
    assert_truncated titleize_freelance_ad('foo' * 100)
  end

  def test_against_job_ad_filters
    JOB_AD_FIXTURES.each do |ad|
      refute titleize_job_ad(ad.text).empty?, "#{ad.text} resulted in empty title"
    end
  end

  def test_against_freelance_ad_filters
    FREELANCE_AD_FIXTURES.each do |ad|
      refute titleize_freelance_ad(ad.text).empty?, "#{ad.text} resulted in empty title"
    end
  end
end
