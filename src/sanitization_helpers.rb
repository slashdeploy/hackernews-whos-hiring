module SanitizationHelpers
  def html_encode(text)
    HTMLEntities.new.encode(text)
  end

  def titleize_job_ad(ad)
    paragraphs = ad.text.split(/($|<p>)/)

    if ad.text.start_with?('<p>') && paragraphs.size > 1
      truncate(sanitize(paragraphs[2]))
    elsif paragraphs.size > 1
      truncate(sanitize(paragraphs.first))
    else
      truncate(sanitize(ad.text))
    end
  end

  def titleize_freelance_ad(ad)
    title = ad.text.
      gsub(/\A.*SEEKING (WORK|FREELANCERS?)/, '').
      gsub(/\A[^a-zA-Z<]*/, '').
      gsub(/\A<p>/, '').
      split('<p>').
      join('; ')

    truncate(title)
  end

  private
  # Remove A tags with content, then everything else
  def strip_tags(text)
    Sanitize.fragment(Sanitize.fragment(text, {
      elements: %w(p pre code b i),
      remove_contents: true
    })).gsub(/\(\s*\)/, '')
  end

  def sanitize(text)
    strip_tags(text).
      split('|').
      map(&:strip).
      reject(&:empty?).
      join(' | ')
  end

  def truncate(string, length: 100, tail: '...')
    if string.length > length - tail.length
      string[0..(length - tail.length)] + tail
    else
      string
    end
  end
end
