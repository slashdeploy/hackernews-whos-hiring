class FreelanceAd < JobAd
  def seeking_work?
    # Some ads do not mention either. They're likely seeking work
    if !text.include?('SEEKING WORK') && !text.include?('SEEKING FREELANCER')
      true
    else
      text.include?('SEEKING WORK')
    end
  end

  def remote?
    !!(text =~ /remote:\s(yes|ok)/i || text =~ /REMOTE/ || text =~ /Remote only/)
  end
end
